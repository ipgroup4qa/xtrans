package ciservice;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import core.Excerpt;

/**
 *
 * @author Alex
 */
public class XMLParser implements DataParser{
    private List<Excerpt> eList = null;
    
    String filePath = null;
    
    public XMLParser(String path) {
    	filePath = null;
	}
    /**
     * Returns the list of excerpts loaded from the xml loaded by the loadData method. 
     * Call this after you call loadData
     * @return the returned list of excerpts
     */
    public List<Excerpt> getExcerpts(){
    	if (eList == null) {
    		loadData();
		}
        return eList;
    }
 
    /**
     * loads the eList object with all the excerpts from the data from a specified valid xml file
     * @param path : a string containing the absolute path to the xml file from which the data is to be loaded
     */
    void loadData(){
        try {
            //declaring some necesarry variables that will load the XML in the DOM format in memory
            File xmlFile = new File(filePath);
            DocumentBuilderFactory docBuildF = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuildF.newDocumentBuilder();
            Document xmlDoc = docBuilder.parse(xmlFile);
            eList = new ArrayList<Excerpt>();
            
            //removes consecutive multiple whitespaces and empty strings from the file
            xmlDoc.getDocumentElement().normalize();
            
            //gets the root of the tree (which should be the "doc" tag)
            Node root = xmlDoc.getDocumentElement();
            
            //makes sure the root is not set to a whitespace from the file (like tab)
            while (root.getNodeType() != Node.ELEMENT_NODE && root != null)
                root = root.getNextSibling();
            if (root == null || root.getNodeName().equals("doc") == false)
                return; //invalid document format
            
            //gets all the children nodes of the "doc" tag (the "text" tags) and iterates through them
            NodeList children = root.getChildNodes();
            for (int i = 0; i < children.getLength(); i++){
                
                //ignores all whitespaces
                if (children.item(i).getNodeType() != Node.ELEMENT_NODE)
                    continue;
                Excerpt newExc = new Excerpt();
                if (children.item(i).hasChildNodes() == false)
                    return; //invalid format
                
                //gets the children of each "text" tag (which should be a "fragment" tag followed by a "rank" tag)
                Node details = children.item(i).getFirstChild();
                while (details.getNodeType() != Node.ELEMENT_NODE && details != null)
                    details = details.getNextSibling();
                if (details == null || details.getNodeName().equals("fragment") == false)
                    return; //invalid format
                newExc.setText(details.getTextContent());
                
                //sets the details variable to the "rank" node
                details = details.getNextSibling();
                while (details.getNodeType() != Node.ELEMENT_NODE && details != null)
                    details = details.getNextSibling();
                if (details == null || details.getNodeName().equals("rank") == false)
                    return;
                
                newExc.setMatch(Integer.parseInt(details.getTextContent().trim()));
                eList.add(newExc);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
