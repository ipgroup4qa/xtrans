package ciservice;

import java.util.List;

import core.Excerpt;

public interface DataParser {
	
	List<Excerpt> getExcerpts();
}
