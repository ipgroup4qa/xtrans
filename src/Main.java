import java.util.List;

import core.Question;
import core.provider.DataProvider;
import core.provider.DataProviderFactory;
import core.provider.XMLDataProvider;


public class Main {

	public static void main(String[] args) {
		//cream un provider
		DataProviderFactory factory = new DataProviderFactory();
		DataProvider provider = factory.create("XMLDataProvider");
		//incarcam xml-ul
		((XMLDataProvider)provider).loadXML("Test.xml");
		//afisam continutul 
		System.out.println(provider.getDocumentFor(null).getRawData());
		List<Question> l = provider.getQuestions();
		for (Question question : l) {
			System.out.println(question);
		}

	}

}
