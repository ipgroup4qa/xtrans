package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tests.ERServiceTest2.StubQuestion;
import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.ERService;
import erservice.JaroDistance;
import erservice.JaroWinklerDistance;
import erservice.StringAlgorithmFactory;
import exceptions.ErException;

public class ERServiceTest4 {

	Question q;
	ERService s;

	@Before
	public void setUp() throws Exception {
		this.q = new StubQuestion();
		this.s = new ERService();
		s.addAlg(new JaroDistance(new StringAlgorithmFactory()));
		s.addAlg(new DiceCoefficient(new StringAlgorithmFactory()));
		s.addAlg(new JaroWinklerDistance(new StringAlgorithmFactory()));
	}

	@Test
	public final void test() {
		try {
			Answer a = null;
			s.extractAnswer(this.q);
			for (Answer answer : this.q.getAnswerList()) {
				if (a == null) {
					a = answer;
				} else if (a.getWeight() < answer.getWeight()) {
					a = answer;
				}

			}
			System.out.println(a.getText());
		} catch (ErException e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	final class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("He had a unique name.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("He was an unusual color."); //CORECT ANSWER
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("He played many different tricks.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("He was kept behind a glass wall.");
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("We had come especially to see the famous works of art, but one day just for a change we went to the zoo.");
			e.setMatch(10);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("His name was Snowflake,&quot; he continued, &quot;and he was a gorilla, a very special albino gorilla, with white fur and pink skin.");
			e.setMatch(20);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("When only three years old, Snowflake was captured in the forests of Africa and then brought to the zoo. ");
			e.setMatch(30);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("Actually, Snowflake was the one who did something.");
			e.setMatch(19);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("Snowflake seemed rather pleased with our reaction, turning away casually and snacking on some leaves.&quot; &quot;");
			e.setMatch(22);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("And Snowflake did live a long life for a gorilla, about forty years, which is equivalent to eighty years for a human.&quot; &quot;So, he's dead now?&quot");
			e.setMatch(20);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("They even made an unscheduled trip to the Barcelona Zoo, where they saw one of Snowflake's grandchildren. ");
			e.setMatch(31);
			this.excerptList.add(e);
		}
	}

}
