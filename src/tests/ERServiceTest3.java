package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tests.ERServiceTest2.StubQuestion;
import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.ERService;
import erservice.JaroDistance;
import erservice.JaroWinklerDistance;
import erservice.StringAlgorithmFactory;
import exceptions.ErException;

public class ERServiceTest3 {

	Question q;
	ERService s;

	@Before
	public void setUp() throws Exception {
		this.q = new StubQuestion();
		this.s = new ERService();
		s.addAlg(new JaroDistance(new StringAlgorithmFactory()));
		s.addAlg(new DiceCoefficient(new StringAlgorithmFactory()));
		s.addAlg(new JaroWinklerDistance(new StringAlgorithmFactory()));
	}

	@Test
	public final void test() {
		try {
			Answer a = null;
			s.extractAnswer(this.q);
			for (Answer answer : this.q.getAnswerList()) {
				if (a == null) {
					a = answer;
				} else if (a.getWeight() < answer.getWeight()) {
					a = answer;
				}

			}
			System.out.println(a.getText());
		} catch (ErException e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	final class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("Meeting many strange people.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Viewing various shocking works of art.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Enjoying very spicy Spanish food.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Encountering an unusual gorilla."); //CORECT ANSWER
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("I need to travel light, you see, so I can buy lots of new dresses and shoes in Paris and Barcelona.");
			e.setMatch(20);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("Have the three of you finally decided what you want to do on your trip?");
			e.setMatch(12);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("Mika and I want to see the wonderful buildings in Barcelona, too.");
			e.setMatch(23);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("I remember when your grandmother and I first went to Barcelona. We went to museums or concerts every day.");
			e.setMatch(20);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("But one of my greatest memories from that trip was completely unexpected.&quot; Grandpa paused again, and then said, &quotWe had come especially to see the famous works of art, but one day just for a change we went to the zoo. ;");
			e.setMatch(45);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("All kinds of wonderful, unexpected encounters may be waiting for you on your trip.&quot;");
			e.setMatch(30);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("They even made an unscheduled trip to the Barcelona Zoo, where they saw one of Snowflake's grandchildren. This baby gorilla didn't have white fur, ");
			e.setMatch(31);
			this.excerptList.add(e);
			
		}
	}

}
