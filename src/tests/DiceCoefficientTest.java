package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.StringAlgorithmFactory;

public class DiceCoefficientTest {

	DiceCoefficient alg;

	@Before
	public void setUp() throws Exception {
		this.alg = new DiceCoefficient(new StringAlgorithmFactory());
	}

	@Test
	public final void testSortAnswers() {
		Question q = new StubQuestion();
		this.alg.sortAnswers(q);
		List<Answer> ex = q.getAnswerList();

		assertEquals(ex.get(0).getWeight(), Integer.valueOf(100));
		assertEquals(ex.get(1).getWeight(), Integer.valueOf(0));
	}

	class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("abcdefghij");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("qqqqqqqqqqq");
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("abcdefghij");
			e.setMatch(100);
			this.excerptList.add(e);
		}
	}

}
