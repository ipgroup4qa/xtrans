package tests;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.ERService;
import erservice.JaroDistance;
import erservice.JaroWinklerDistance;
import erservice.StringAlgorithmFactory;
import exceptions.ErException;

public class ERServiceTest {
	Question q;
	ERService s;

	@Before
	public void setUp() throws Exception {
		this.q = new StubQuestion();
		this.s = new ERService();
		s.addAlg(new JaroDistance(new StringAlgorithmFactory()));
		s.addAlg(new DiceCoefficient(new StringAlgorithmFactory()));
		s.addAlg(new JaroWinklerDistance(new StringAlgorithmFactory()));
	}

	@Test
	public final void test() {
		try {
			Answer a = null;
			s.extractAnswer(this.q);
			for (Answer answer : this.q.getAnswerList()) {
				if (a == null) {
					a = answer;
				} else if (a.getWeight() < answer.getWeight()) {
					a = answer;
				}

			}
			System.out.println(a.getText());
		} catch (ErException e) {
			System.out.println(e.getStackTrace());
		}
	}

	final class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("Valerie had not finished her preparation.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Valerie had too many things in her suitcase.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("They both knew that what Valerie said was not true.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("They both understood that Valerie had very little money.");
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("Valerie asked.Yes, he died of skin cancer in 2003.");
			e.setMatch(5);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("I am just falling asleep over the pictures,&quot; he said, laughing");
			e.setMatch(60);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("They both laughed because Valerie was not actually interested in fashion at all. She loved foreign languages, music, art, good food, and many other things - but not shopping for clothes.");
			e.setMatch(90);
			this.excerptList.add(e);
		}
	}

}
