package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tests.ERServiceTest.StubQuestion;
import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.ERService;
import erservice.JaroDistance;
import erservice.JaroWinklerDistance;
import erservice.StringAlgorithmFactory;
import exceptions.ErException;

public class ERServiceTest2 {

	Question q;
	ERService s;

	@Before
	public void setUp() throws Exception {
		this.q = new StubQuestion();
		this.s = new ERService();
		s.addAlg(new JaroDistance(new StringAlgorithmFactory()));
		s.addAlg(new DiceCoefficient(new StringAlgorithmFactory()));
		s.addAlg(new JaroWinklerDistance(new StringAlgorithmFactory()));
	}

	@Test
	public final void test() {
		try {
			Answer a = null;
			s.extractAnswer(this.q);
			for (Answer answer : this.q.getAnswerList()) {
				if (a == null) {
					a = answer;
				} else if (a.getWeight() < answer.getWeight()) {
					a = answer;
				}

			}
			System.out.println(a.getText());
		} catch (ErException e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	final class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("Go to the zoo.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Travel light.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Eat authentic dishes.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Don't plan everything."); //CORECT ANSWER
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("But, basically, we still don't have much of a plan.");
			e.setMatch(30);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("You know, sometimes it may be best not to have a detailed plan. ");
			e.setMatch(59);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText(", Grandpa smiled at Valerie and said,\"Anyway, maybe it's best not to plan everything.\"");
			e.setMatch(72);
			this.excerptList.add(e);
		}
	}

}
