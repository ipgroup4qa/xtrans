package tests;

import static org.junit.Assert.*;
import library.similarity.StringSimilarityService;

import org.junit.Before;
import org.junit.Test;

import erservice.StringAlgorithmFactory;
import erservice.StringAlgorithmFactoryInterface;

public class SimilarityAlgorithmFactoryTest {
	StringAlgorithmFactoryInterface factory;

	@Before
	public void setUp() throws Exception {
		this.factory = new StringAlgorithmFactory();
	}

	@Test
	public final void testJaroWinkler() {
		StringSimilarityService service = this.factory.create("JaroWinkler");
		assertNotEquals(null, service);
		assertEquals("StringSimilarityServiceImpl", service.getClass()
				.getSimpleName());
	}

	@Test
	public final void testJaro() {
		StringSimilarityService service = this.factory.create("Jaro");
		assertNotEquals(null, service);
		assertEquals("StringSimilarityServiceImpl", service.getClass()
				.getSimpleName());
	}

	@Test
	public final void testDiceCoefficient() {
		StringSimilarityService service = this.factory
				.create("DiceCoefficient");
		assertNotEquals(null, service);
		assertEquals("StringSimilarityServiceImpl", service.getClass()
				.getSimpleName());
	}

	@Test
	public final void testFail() {
		StringSimilarityService service = this.factory
				.create("ThisMustReturnNull");
		assertEquals(null, service);
	}

}
