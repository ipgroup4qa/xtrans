package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import tests.ERServiceTest2.StubQuestion;
import core.Answer;
import core.Excerpt;
import core.Question;
import erservice.DiceCoefficient;
import erservice.ERService;
import erservice.JaroDistance;
import erservice.JaroWinklerDistance;
import erservice.StringAlgorithmFactory;
import exceptions.ErException;

public class ERServiceTest5 {
	
	Question q;
	ERService s;

	@Before
	public void setUp() throws Exception {
		this.q = new StubQuestion();
		this.s = new ERService();
		s.addAlg(new JaroDistance(new StringAlgorithmFactory()));
		s.addAlg(new DiceCoefficient(new StringAlgorithmFactory()));
		s.addAlg(new JaroWinklerDistance(new StringAlgorithmFactory()));
	}

	@Test
	public final void test() {
		try {
			Answer a = null;
			s.extractAnswer(this.q);
			for (Answer answer : this.q.getAnswerList()) {
				if (a == null) {
					a = answer;
				} else if (a.getWeight() < answer.getWeight()) {
					a = answer;
				}

			}
			System.out.println(a.getText());
		} catch (ErException e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	final class StubQuestion extends Question {

		public StubQuestion() {
			this.answerList = new ArrayList<Answer>();
			Answer answer;
			answer = new Answer();
			answer.setText("There is a high risk of getting skin cancer.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Air pollution causes damage to their health.");
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("They are threatened by human violence.");//CORECT ANSWER
			this.answerList.add(answer);
			answer = new Answer();
			answer.setText("Lions and other animals commonly attack them."); 
			this.answerList.add(answer);

			this.excerptList = new ArrayList<Excerpt>();
			Excerpt e;
			e = new Excerpt();
			e.setText("And there I met someone who had a great effect on me.&quot; &quot;His name was Snowflake,&quot;he continued, &quot;and he was a gorilla, a very special albino gorilla, with white fur and pink skin.");
			e.setMatch(10);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("When only three years old, Snowflake was captured in the forests of Africa and then brought to the zoo.");
			e.setMatch(20);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("He had very pale skin, of course, so sunlight was very dangerous for him.");
			e.setMatch(15);
			this.excerptList.add(e);
			e = new Excerpt();
			e.setText("But in the wild so many gorillas are killed by hunters or disease that it is difficult to say which way of life is better");
			e.setMatch(11);
			this.excerptList.add(e);
		}
	}

}
