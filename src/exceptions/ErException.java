/**
 * 
 */
package exceptions;

/**
 * @author Maftei Robert
 * 
 *         Generic exception class for ErModule
 */
public class ErException extends Exception {
	private static final long serialVersionUID = 6908177146933864217L;

	/**
	 * 
	 */
	public ErException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public ErException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public ErException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ErException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public ErException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
