package core.document;

public class AbstractDocumentFactory {
	
	public Document createTextDocument(String type) {
		if (type.equals("CompositeDocument")) {
			return new CompositeDocument();
		} else if (type.equals("TextDocument")) {
			return new TextDocument();
		}
		return null;
	}

}
