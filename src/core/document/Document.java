package core.document;

import core.textformat.TextElement;

public interface Document extends TextElement{
	public String getRawData();
}
