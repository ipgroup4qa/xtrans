package core;

import java.util.*;

import core.textformat.TextElement;
import core.textformat.TextElementVisitor;

public class Question implements TextElement {

	protected List<Answer> answerList;

	protected String text;

	protected Keywords keywords;

	protected List<Excerpt> excerptList;

	public List<Answer> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<Answer> answerList) {		this.answerList = answerList;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Keywords getKeywords() {
		return keywords;
	}

	public void setKeywords(Keywords keywords) {
		this.keywords = keywords;
	}

	public List<Excerpt> getExcerptList() {
		return excerptList;
	}

	public void setExcerptList(List<Excerpt> excerptList) {
		this.excerptList = excerptList;
	}

	@Override
	public void accept(TextElementVisitor visitor) {

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: "+this.text+"\n");
		for (Answer ans : this.answerList) {
			sb.append(ans+"\n");
		}
		return sb.toString();
	}

}
