package core.textformat;


public interface TextElement {

	public void accept(TextElementVisitor visitor);

}
