package core.textformat;

import core.Answer;
import core.Question;
import core.document.Document;

public interface TextElementVisitor {

	public String visit(Question question);

	public String visit(Document doc);
	
	public String visit(Answer ans);

}