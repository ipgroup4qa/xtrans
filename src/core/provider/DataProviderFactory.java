package core.provider;

public class DataProviderFactory {
	
	 public  DataProvider create(String type) {
		 if (type.equals("XMLDataProvider")) {
			 return new XMLDataProvider();
		 }
		 return null;
	 }

}
