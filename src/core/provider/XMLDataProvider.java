package core.provider;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import core.Answer;
import core.Question;
import core.document.AbstractDocumentFactory;
import core.document.TextDocument;

public class XMLDataProvider implements DataProvider {

	public core.document.Document doc;

	public List<Question> qList;

	@Override
	public core.document.Document getDocumentFor(Question question) {
		
		return doc;
	}

	@Override
	public List<Question> getQuestions() {
		// TODO Auto-generated method stub
		return this.qList;
	}

	public void loadXML(String resource) {
		  try {
			  
				File fXmlFile = new File(resource);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
			 
				doc.getDocumentElement().normalize();
				//luam elementele doc, ar trebui sa fie doar unul singur, in caz ca sint mai multe aruncam exceptie
				NodeList dList = doc.getElementsByTagName("doc");
				if (dList.getLength() != 1) throw new Exception("Invalid format for the document");
				//seteam documentul text
				String text = dList.item(0).getTextContent();
				//folosim factory pentru a crea
				AbstractDocumentFactory documentFactory = new AbstractDocumentFactory();
				this.doc = documentFactory.createTextDocument("TextDocument");
				((TextDocument)this.doc).setData(text);
				//acum trebuie sa cream lista cu intrebari
				//luam mai intii toate elementele de tip intrebare
				NodeList nList = doc.getElementsByTagName("question");
				List<Question> listQuestions = new ArrayList<Question>();
				//pentru fiecare element intreabre se va executa codul asta
				for (int temp = 0; temp < nList.getLength(); temp++) { 
					Node nQuestionNode = nList.item(temp);
					NodeList questionChildren = nQuestionNode.getChildNodes();
					String questionString = null;
					List<Answer> listAnswers = new ArrayList<Answer>();
					for (int i = 0; i < questionChildren.getLength(); i++) {
						Node node = questionChildren.item(i); 
						if (node.getNodeName().equals("q_str")){
							questionString = node.getTextContent();
						} else if (node.getNodeName().equals("answer")) {
							Answer ans = new Answer();
							ans.setText(node.getTextContent());
							listAnswers.add(ans);
						}
					}
					Question quest = new Question();
					quest.setAnswerList(listAnswers);
					quest.setText(questionString);
					listQuestions.add(quest);
				}
				this.qList = listQuestions;	
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
	}

}
