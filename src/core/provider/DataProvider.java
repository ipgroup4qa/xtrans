package core.provider;

import java.util.List;

import core.Question;
import core.document.Document;

public interface DataProvider {

	public List<Question> getQuestions();

	public Document getDocumentFor(Question question);

}
