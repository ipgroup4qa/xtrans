package core;

import core.textformat.TextElement;
import core.textformat.TextElementVisitor;

public class Answer implements TextElement {
	
	 public Integer weight;

	 public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String text;

	@Override
	public void accept(TextElementVisitor visitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "Answer : "+text+"; weight : "+weight;
	}

}
