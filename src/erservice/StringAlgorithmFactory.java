/**
 * 
 */
package erservice;

import library.similarity.DiceCoefficientStrategy;
import library.similarity.JaroStrategy;
import library.similarity.JaroWinklerStrategy;
import library.similarity.StringSimilarityService;
import library.similarity.StringSimilarityServiceImpl;

/**
 * @author Maftei Robert
 * 
 */
public class StringAlgorithmFactory implements StringAlgorithmFactoryInterface {

	/*
	 * (non-Javadoc)
	 * 
	 * @see erservice.StringAlgorithmFactoryInterface#create(java.lang.String)
	 */
	@Override
	public StringSimilarityService create(String type) {
		if (type.equals("JaroWinkler")) {
			return new StringSimilarityServiceImpl(new JaroWinklerStrategy());
		} else if (type.equals("Jaro")) {
			return new StringSimilarityServiceImpl(new JaroStrategy());
		} else if (type.equals("DiceCoefficient")) {
			return new StringSimilarityServiceImpl(
					new DiceCoefficientStrategy());
		}

		return null;
	}

}
