/**
 * 
 */
package erservice;

import exceptions.ErException;

/**
 * @author Maftei Robert
 * 
 *         Sorting with Jaro Winkler Distance algorithm
 */
public class JaroWinklerDistance extends StrategyERAlgorithm {

	public JaroWinklerDistance(StringAlgorithmFactoryInterface factory)
			throws ErException {
		super(factory);
	}

	protected void setService(StringAlgorithmFactoryInterface factory)
			throws ErException {
		this.service = factory.create("JaroWinkler");
		if (this.service == null) {
			throw new ErException("Algorithm not found");
		}
	}
}
