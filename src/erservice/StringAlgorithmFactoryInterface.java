package erservice;

import library.similarity.StringSimilarityService;

public interface StringAlgorithmFactoryInterface {
	public StringSimilarityService create(String type);
}
