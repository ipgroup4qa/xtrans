package erservice;

import java.util.ArrayList;
import java.util.List;

import library.similarity.StringSimilarityService;
import core.Answer;
import core.Excerpt;
import core.Question;
import exceptions.ErException;

public abstract class StrategyERAlgorithm implements ERAlgorithm {

	public StringSimilarityService service;

	public StrategyERAlgorithm(StringAlgorithmFactoryInterface factory)
			throws ErException {
		this.setService(factory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see erservice.ERAlgorithm#sortAnswers(core.Question)
	 */
	@Override
	public void sortAnswers(Question q) {
		List<Excerpt> excerpts = q.getExcerptList();
		List<Answer> answers = q.getAnswerList();
		List<Double> scores = new ArrayList<Double>();
		for (Answer answer : answers) {
			double finalScore = 0.0;
			for (Excerpt excerpt : excerpts) {
				double score = this.service.score(answer.getText(),
						excerpt.getText());
				Integer match = 0;
				if (excerpt.getMatch() == null) {
					match = 1;
				} else {
					match = excerpt.getMatch();
				}
				finalScore += score * 100 * match;
			}
			scores.add(finalScore);
		}
		// 100 points max for each excerpt * 100 points max for each match
		Double maxPoints = (double) (excerpts.size() * 100) * 100;
		for (int i = 0; i < scores.size(); i++) {
			Double score = scores.get(i);
			Double finalScore = (score * 100) / maxPoints;
			answers.get(i).setWeight(finalScore.intValue());
		}
	}
	
	abstract protected void setService(StringAlgorithmFactoryInterface factory) throws ErException;

}
