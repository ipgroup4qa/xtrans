/**
 * 
 */
package erservice;

import exceptions.ErException;

/**
 * @author Maftei
 * 
 */
public class JaroDistance extends StrategyERAlgorithm {

	/**
	 * @param factory
	 * @throws ErException
	 */
	public JaroDistance(StringAlgorithmFactoryInterface factory)
			throws ErException {
		super(factory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see erservice.StrategyERAlgorithm#setService(erservice.
	 * StringAlgorithmFactoryInterface)
	 */
	@Override
	protected void setService(StringAlgorithmFactoryInterface factory)
			throws ErException {
		this.service = factory.create("Jaro");
		if (this.service == null) {
			throw new ErException("Algorithm not found");
		}
	}

}
