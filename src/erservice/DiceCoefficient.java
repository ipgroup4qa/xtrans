/**
 * 
 */
package erservice;

import exceptions.ErException;

/**
 * @author Maftei
 * 
 */
public class DiceCoefficient extends StrategyERAlgorithm {

	/**
	 * @param factory
	 * @throws ErException
	 */
	public DiceCoefficient(StringAlgorithmFactoryInterface factory)
			throws ErException {
		super(factory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see erservice.StrategyERAlgorithm#setService(erservice.
	 * StringAlgorithmFactoryInterface)
	 */
	@Override
	protected void setService(StringAlgorithmFactoryInterface factory)
			throws ErException {
		this.service = factory.create("DiceCoefficient");
		if (this.service == null) {
			throw new ErException("Algorithm not found");
		}

	}

}
