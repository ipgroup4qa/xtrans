package erservice;

import core.Question;

public interface ERAlgorithm {
	
	public void sortAnswers(Question q);
}
