package erservice;

import java.util.ArrayList;
import java.util.List;

import exceptions.ErException;
import core.Answer;
import core.Question;

public class ERService {

	public List<ERAlgorithm> algList;
	
	public ERService()
	{
		this.algList = new ArrayList<ERAlgorithm>();
	}

	public void extractAnswer(Question q) throws ErException {
		if (this.algList.size() == 0) {
			throw new ErException("No er algorithm was set");
		}

		List<Double> scores = new ArrayList<Double>();
		for (int i = 0; i < q.getAnswerList().size(); i++) {
			scores.add(0.0);
		}

		for (ERAlgorithm alg : this.algList) {
			alg.sortAnswers(q);
			Integer i = 0;
			for (Answer answer : q.getAnswerList()) {
				scores.set(i,
						(scores.get(i) + answer.getWeight().doubleValue()));
				i++;
			}
			this.resetWeight(q);
		}

		Integer i = 0;
		for (Answer answer : q.getAnswerList()) {
			answer.setWeight((int) (scores.get(i) / this.algList.size()));
			i = i + 1;
		}
	}

	public void addAlg(ERAlgorithm alg) throws ErException {
		if (this.algList.contains(alg)) {
			throw new ErException("Algorithm already present in the list");
		}
		this.algList.add(alg);
	}

	public void removeAlg(ERAlgorithm alg) throws ErException {
		if (!this.algList.contains(alg)) {
			throw new ErException("Algorithm not found in the list");
		}
		this.algList.remove(alg);
	}

	/**
	 * reset answers weight after each algorithm
	 * 
	 * @param question
	 */
	protected void resetWeight(Question q) {
		for (Answer answer : q.getAnswerList()) {
			answer.setWeight(null);
		}
	}
}
